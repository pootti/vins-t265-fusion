/*******************************************************
 * Copyright (C) 2019, Aerial Robotics Group, Hong Kong University of Science and Technology
 *
 * This file is part of VINS.
 *
 * Licensed under the GNU General Public License v3.0;
 * you may not use this file except in compliance with the License.
 *
 * Author: Qin Tong (qintonguav@gmail.com)
 *******************************************************/

#include <stdio.h>
#include <queue>
#include <map>
#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <ros/package.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include "estimator/estimator.h"
#include "estimator/parameters.h"
#include "utility/visualization.h"
#include "featureTracker/feature_tracker.h"

Estimator estimator;

queue<sensor_msgs::ImuConstPtr> imu_buf;
queue<sensor_msgs::PointCloudConstPtr> feature_buf;
queue<sensor_msgs::CompressedImageConstPtr> img0_buf;
queue<sensor_msgs::CompressedImageConstPtr> img1_buf;
queue<sensor_msgs::CompressedImageConstPtr> agent_img0_buf; //use m_buf for lock
queue<sensor_msgs::CompressedImageConstPtr> agent_img1_buf;
queue<agent_msg::AgentMsg> agent_msg_buf;
std::mutex m_buf;
std::mutex m_agent_msg_buf;

std::string BRIEF_PATTERN_FILE;
Vector3d last_agent_t(0,0,0);
vector<camodocal::CameraPtr> m_camera;


void img0_callback(const sensor_msgs::CompressedImageConstPtr &img_msg)
{
    m_buf.lock();
    img0_buf.push(img_msg);
    agent_img0_buf.push(img_msg);
    m_buf.unlock();
}

void img1_callback(const sensor_msgs::CompressedImageConstPtr &img_msg)
{
    m_buf.lock();
    img1_buf.push(img_msg);
    agent_img1_buf.push(img_msg);
    m_buf.unlock();
}


cv::Mat getImageFromMsg(const sensor_msgs::CompressedImageConstPtr &img_msg)
{
    cv_bridge::CvImageConstPtr ptr;
    // if (img_msg->encoding == "8UC1")
    // {
    //     sensor_msgs::Image img;
    //     img.header = img_msg->header;
    //     img.height = img_msg->height;
    //     img.width = img_msg->width;
    //     img.is_bigendian = img_msg->is_bigendian;
    //     img.step = img_msg->step;
    //     img.data = img_msg->data;
    //     img.encoding = "mono8";
    //     ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);
    // }
    // else
    ptr = cv_bridge::toCvCopy(img_msg, sensor_msgs::image_encodings::MONO8);

    cv::Mat img = ptr->image.clone();
    return img;
}

// extract images with same timestamp from two topics
void sync_process()
{
    while(1)
    {
        std_msgs::Header header;
        std_msgs::Header agent_header;
        if(STEREO)
        {
            cv::Mat image0, image1;

            double time = 0;
            m_buf.lock();
            if (!img0_buf.empty() && !img1_buf.empty())
            {
                double time0 = img0_buf.front()->header.stamp.toSec();
                double time1 = img1_buf.front()->header.stamp.toSec();
                // 0.003s sync tolerance
                if(time0 < time1 - 0.003)
                {
                    img0_buf.pop();
                    printf("throw img0\n");
                }
                else if(time0 > time1 + 0.003)
                {
                    img1_buf.pop();
                    printf("throw img1\n");
                }
                else
                {
                    time = img0_buf.front()->header.stamp.toSec();
                    header = img0_buf.front()->header;
                    image0 = getImageFromMsg(img0_buf.front());
                    img0_buf.pop();
                    image1 = getImageFromMsg(img1_buf.front());
                    img1_buf.pop();
                    agent_header=img0_buf.back()->header;
                    //printf("find img0 and img1\n");
                }
            }
            m_buf.unlock();
            if(!image0.empty())
                estimator.inputImage(time, image0, image1);
        }
        else
        {
            cv::Mat image;
            std_msgs::Header header;
            double time = 0;
            m_buf.lock();
            if(!img0_buf.empty())
            {
                time = img0_buf.front()->header.stamp.toSec();
                header = img0_buf.front()->header;
                image = getImageFromMsg(img0_buf.front());
                img0_buf.pop();
            }
            m_buf.unlock();
            if(!image.empty())
                estimator.inputImage(time, image);
        }
        if (SWARM_AGENT)
            {
                if (estimator.solver_flag == Estimator::SolverFlag::NON_LINEAR && estimator.marginalization_flag == 0)//Old keyframe?
                {
                    Vector3d tmp_agent_t = estimator.Ps[WINDOW_SIZE - 2];
                    if ((tmp_agent_t - last_agent_t).norm() > 0.05)
                    {	
                        TicToc pubAgentFrame_time;
                        agent_msg::AgentMsg agent_frame_msg;
                        preprocessAgentFrame(estimator, agent_frame_msg,agent_header);
                        m_agent_msg_buf.lock();
                        agent_msg_buf.push(agent_frame_msg);
                        m_agent_msg_buf.unlock();
                        last_agent_t = tmp_agent_t;
                    }
                }
            }

        std::chrono::milliseconds dura(2);
        std::this_thread::sleep_for(dura);
    }
}


void imu_callback(const sensor_msgs::ImuConstPtr &imu_msg)
{
    double t = imu_msg->header.stamp.toSec();
    double dx = imu_msg->linear_acceleration.x;
    double dy = imu_msg->linear_acceleration.y;
    double dz = imu_msg->linear_acceleration.z;
    double rx = imu_msg->angular_velocity.x;
    double ry = imu_msg->angular_velocity.y;
    double rz = imu_msg->angular_velocity.z;
    Vector3d acc(dx, dy, dz);
    Vector3d gyr(rx, ry, rz);
    estimator.inputIMU(t, acc, gyr);
    return;
}


void feature_callback(const sensor_msgs::PointCloudConstPtr &feature_msg)
{
    map<int, vector<pair<int, Eigen::Matrix<double, 7, 1>>>> featureFrame;
    for (unsigned int i = 0; i < feature_msg->points.size(); i++)
    {
        int feature_id = feature_msg->channels[0].values[i];
        int camera_id = feature_msg->channels[1].values[i];
        double x = feature_msg->points[i].x;
        double y = feature_msg->points[i].y;
        double z = feature_msg->points[i].z;
        double p_u = feature_msg->channels[2].values[i];
        double p_v = feature_msg->channels[3].values[i];
        double velocity_x = feature_msg->channels[4].values[i];
        double velocity_y = feature_msg->channels[5].values[i];
        if(feature_msg->channels.size() > 5)
        {
            double gx = feature_msg->channels[6].values[i];
            double gy = feature_msg->channels[7].values[i];
            double gz = feature_msg->channels[8].values[i];
            pts_gt[feature_id] = Eigen::Vector3d(gx, gy, gz);
            //printf("receive pts gt %d %f %f %f\n", feature_id, gx, gy, gz);
        }
        ROS_ASSERT(z == 1);
        Eigen::Matrix<double, 7, 1> xyz_uv_velocity;
        xyz_uv_velocity << x, y, z, p_u, p_v, velocity_x, velocity_y;
        featureFrame[feature_id].emplace_back(camera_id,  xyz_uv_velocity);
    }
    double t = feature_msg->header.stamp.toSec();
    estimator.inputFeature(t, featureFrame);
    return;
}

void restart_callback(const std_msgs::BoolConstPtr &restart_msg)
{
    if (restart_msg->data == true)
    {
        ROS_WARN("restart the estimator!");
        estimator.clearState();
        estimator.setParameter();
    }
    return;
}

void imu_switch_callback(const std_msgs::BoolConstPtr &switch_msg)
{
    if (switch_msg->data == true)
    {
        //ROS_WARN("use IMU!");
        estimator.changeSensorType(1, STEREO);
    }
    else
    {
        //ROS_WARN("disable IMU!");
        estimator.changeSensorType(0, STEREO);
    }
    return;
}

void cam_switch_callback(const std_msgs::BoolConstPtr &switch_msg)
{
    if (switch_msg->data == true)
    {
        //ROS_WARN("use stereo!");
        estimator.changeSensorType(USE_IMU, 1);
    }
    else
    {
        //ROS_WARN("use mono camera (left)!");
        estimator.changeSensorType(USE_IMU, 0);
    }
    return;
}

// For t265
void t265_callback(const nav_msgs::OdometryConstPtr &t265_msg)
{
  geometry_msgs::Pose tmp_pose;
  tmp_pose = t265_msg -> pose.pose;

  double t = t265_msg -> header.stamp.toSec();
  Eigen::Vector3d tmp_position;

  tmp_position[0] = tmp_pose.position.x;
  tmp_position[1] = tmp_pose.position.y;
  tmp_position[2] = tmp_pose.position.z;

  estimator.inputt265(t, tmp_position);

  return;
}
void agent_process()
{
    int i = 0;
    while(1)
    {
        m_agent_msg_buf.lock();
        agent_msg::AgentMsg tmp_msg;
        bool pub_flag = false;
        while (!agent_msg_buf.empty())
        {
            tmp_msg = agent_msg_buf.front();
            agent_msg_buf.pop();
            pub_flag = true;
        }
        m_agent_msg_buf.unlock();
	
        if (pub_flag)
        {
	    if(i==0){
		    TicToc pubAgentFrame_time;
		    sensor_msgs::CompressedImageConstPtr image_msg = NULL;
		    m_buf.lock();
		    while(!agent_img0_buf.empty() && agent_img0_buf.front()->header.stamp.toSec() < tmp_msg.header.stamp.toSec())//Pop image buffer until the one that is after the agent message
			agent_img0_buf.pop();
		    if (!agent_img0_buf.empty())
		    {
			image_msg = agent_img0_buf.front();
		    }
		    m_buf.unlock();
		    //ROS_INFO("image_msg sec: %f",image_msg->header.stamp.toSec());
		    //ROS_INFO("tmp_msg sec: %f",tmp_msg.header.stamp.toSec());
		    if (image_msg == NULL || image_msg->header.stamp.toSec() != tmp_msg.header.stamp.toSec())
		    {
			    ROS_WARN("can not find corresponding image");
		    }
		    else
		    {
			cv_bridge::CvImageConstPtr ptr;
			// if (image_msg->encoding == "8UC1")
			// {
			//     sensor_msgs::Image img;
			//     img.header = image_msg->header;
			//     img.height = image_msg->height;
			//     img.width = image_msg->width;
			//     img.is_bigendian = image_msg->is_bigendian;
			//     img.step = image_msg->step;
			//     img.data = image_msg->data;
			//     img.encoding = "mono8";
			//     ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);
			// }
			// else
			//     
            		ptr = cv_bridge::toCvCopy(image_msg, sensor_msgs::image_encodings::MONO8);

			cv::Mat img = ptr->image;
			pubAgentFrame(tmp_msg, img, m_camera[0]); //Send agent_message, image, and camera parameter
		    }
	    	    i=1;
	    }
	    else if (i==1){
		    //Do it again for image1
		    TicToc pubAgentFrame_time;
		    sensor_msgs::CompressedImageConstPtr image_msg = NULL;
		    m_buf.lock();
		    while(!agent_img1_buf.empty() && agent_img1_buf.front()->header.stamp.toSec() < tmp_msg.header.stamp.toSec())//Pop image buffer until the one that is after the agent message
			agent_img1_buf.pop();
		    if (!agent_img1_buf.empty())
		    {
			image_msg = agent_img1_buf.front();
		    }
		    m_buf.unlock();
		    if (image_msg == NULL || image_msg->header.stamp.toSec() != tmp_msg.header.stamp.toSec())
		    {
			    ROS_WARN("can not find corresponding image");
		    }
		    else
		    {
			cv_bridge::CvImageConstPtr ptr;
			// if (image_msg->encoding == "8UC1")
			// {
			//     sensor_msgs::Image img;
			//     img.header = image_msg->header;
			//     img.height = image_msg->height;
			//     img.width = image_msg->width;
			//     img.is_bigendian = image_msg->is_bigendian;
			//     img.step = image_msg->step;
			//     img.data = image_msg->data;
			//     img.encoding = "mono8";
			//     ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);
			// }
			// else
		    ptr = cv_bridge::toCvCopy(image_msg, sensor_msgs::image_encodings::MONO8);

			cv::Mat img = ptr->image;
			pubAgentFrame(tmp_msg, img, m_camera[1]);
		    }
		    i=0;
	    }
        }
        std::chrono::milliseconds dura(5);
        std::this_thread::sleep_for(dura);
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "vins_estimator");
    ros::NodeHandle n("~");
    ros::NodeHandle nh;
    ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info);

    if(argc != 2)
    {
        printf("please intput: rosrun vins vins_node [config file] \n"
               "for example: rosrun vins vins_node "
               "~/catkin_ws/src/VINS-Fusion/config/euroc/euroc_stereo_imu_config.yaml \n");
        return 1;
    }

    string config_file = argv[1];
    printf("config_file: %s\n", argv[1]);

    readParameters(config_file,n,nh);
    estimator.setParameter();

#ifdef EIGEN_DONT_PARALLELIZE
    ROS_DEBUG("EIGEN_DONT_PARALLELIZE");
#endif

    ROS_WARN("waiting for image and imu...");

    registerPub(n,nh);

    ros::Subscriber sub_imu;
    if(USE_IMU)
    {
        sub_imu = n.subscribe(IMU_TOPIC, 2000, imu_callback, ros::TransportHints().tcpNoDelay());
    }
    ros::Subscriber sub_feature = n.subscribe("/feature_tracker/feature", 2000, feature_callback);
    ros::Subscriber sub_img0 = n.subscribe(IMAGE0_TOPIC, 100, img0_callback);
    ros::Subscriber sub_img1;
    if(STEREO)
    {
        sub_img1 = n.subscribe(IMAGE1_TOPIC, 100, img1_callback);
    }
    ros::Subscriber sub_restart = n.subscribe("/vins_restart", 100, restart_callback);
    ros::Subscriber sub_imu_switch = n.subscribe("/vins_imu_switch", 100, imu_switch_callback);
    ros::Subscriber sub_cam_switch = n.subscribe("/vins_cam_switch", 100, cam_switch_callback);

    // For t265
    ros::Subscriber sub_t265 = n.subscribe(T265_TOPIC, 2000, t265_callback);
    
    std::thread agent_process_thread;
    std::thread sync_thread{sync_process};

    if (SWARM_AGENT)
    {
        ROS_INFO("start swarm mode");
        std::string pkg_path = ros::package::getPath("vins");
        BRIEF_PATTERN_FILE = pkg_path + "/../support_files/brief_pattern.yml";
        agent_process_thread = std::thread(agent_process);
    }



    ros::spin();

    return 0;
}
