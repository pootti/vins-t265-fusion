/*******************************************************
 * Copyright (C) 2021, Urban Robotics Lab, Korea Advanced Institute of Science and Technology (KAIST)
 *
 * This file is part of VINS.
 *
 * Licensed under the GNU General Public License v3.0;
 * you may not use this file except in compliance with the License.
 *******************************************************/

#include <ceres/ceres.h>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include <cmath>
#include <typeinfo>

#include "../estimator/parameters.h"
#include "../utility/utility.h"

struct T265Factor
{
  T265Factor(Eigen::Vector3d P) : t265_position(P){}

  template <typename T>
  bool operator()(const T* est_position, T* residual) const
  {
    const T x_diff = t265_position[0] - est_position[0];
    const T y_diff = t265_position[1] - est_position[1];
    const T z_diff = t265_position[2] - est_position[2];

    residual[0] = x_diff;
    residual[1] = y_diff;
    residual[2] = z_diff;

    return true;
  }
private:
Eigen::Vector3d t265_position;
};
